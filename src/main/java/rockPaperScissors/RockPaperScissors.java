package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String input = "";
    String plChoice = "";
    Boolean playing = true;
    Boolean inRound = true;
    
    public void run() {
        //Checking if the player choice lost or won
        String winCon = "";
        if (plChoice.equals("paper")) {
        	winCon = "rock";
        } else if (plChoice.equals("rock")) {
        	winCon = "scissors";
        } else {
        	winCon = "paper";
        }
        
        //Getting player choice and checking if it is valid
        while (playing) {
        	System.out.println("Let's play round " + roundCounter);
        	input = readInput("Your choice (Rock/Paper/Scissors)?");
            plChoice = input.toLowerCase();
            inRound = true;
            
            while (inRound) {
	        	if (rpsChoices.contains(plChoice)) {
	        		//Computer making a choice
	                int rng = (int) (Math.random() * 3);
	                String cpChoice = rpsChoices.get(rng);
	                
	        		System.out.print("Human chose " + plChoice + ", computer chose " + cpChoice + ". ");
	        		
	        		//Determines the outcome of the game
	        		if (cpChoice.equals(winCon)) {
	        			System.out.println("Human wins!");
	        			humanScore++;
	        		} else if (plChoice.contentEquals(cpChoice)) {
	        			System.out.println("It's a tie!");
	        		} else {
	        			computerScore++;
	        			System.out.println("Computer wins!");
	        		}
	        		
	        		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
	        		
	        		
	        		roundCounter++;
	        		
	        		// asking if the player wants to play again
	        		input = readInput("Do you wish to continue playing? (y/n)?");
	        		if (input.equalsIgnoreCase("y")) {
	        			
	        		} else if (input.equalsIgnoreCase("n")){
	        			System.out.println("Bye bye :)");
	        			playing = false;
	        		} else {
	        			input = readInput("Do you wish to play again? (y/n)?");
	        		}
	        		inRound = false;
	        	} else {
        			input = readInput("I do not understand " + input + ". Could you try again?");
        			plChoice = input.toLowerCase();
        		}
        	}
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
